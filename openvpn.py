#!/usr/bin/python
# -*- coding: utf-8 -*-

import getopt
import os
import subprocess
import sys


def main(argv):
    ovpndir = 'xx'  # full path to ovpn directory
    credentials = 'xx'  # full path to login credentials

    # parse any arguments presented
    try:
        opts, args = getopt.getopt(argv, 'hf:c:')
    except getopt.GetoptError:
        print ('openvpn.py -f <path to ovpn folder> -c <path to credentials file>')
        exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('openvpn.py -f <path to ovpn folder> -c <path to credentials file>')
            exit()
        elif opt == '-f':
            ovpndir = arg
        elif opt == '-c':
            credentials = arg

    # set default values for missing arguments
    if ovpndir == 'xx':
        ovpndir = '/etc/openvpn'

    if credentials == 'xx':
        credentials = '/etc/openvpn/login.conf'

    # check if ovpn directory and credentials file exist
    if not os.path.isdir(ovpndir):
        print('No ovpn directory found')
        exit(0)

    if not os.path.exists(credentials):
        print('No credentials file found')
        exit(0)

    # collect all files ending in .ovpn
    ovpns = [file for file in os.listdir(ovpndir) if '.ovpn' in file]

    if len(ovpns) == 0:
        print('No .ovpn files found in {}'.format(ovpndir))
        exit(0)

    print('\nSelect OpenVPN Location\n')

    # print numbered list of vpn files found
    cnt = 1
    line = ''
    vpn_locations = []
    for ovpn in ovpns:
        (filename, extension) = os.path.splitext(ovpn)
        vpn_locations.append(filename)
        line += '{}. {}'.format(cnt, filename).ljust(30)
        if cnt % 2 == 0:
            print(line)
            line = ''
        cnt += 1
    if line:
        print(line)

    # read and check for valid user vpn  selection
    val = 0
    while val <1 or val >= cnt:
        val = int(input('\nEnter Location Number [1-{}] > '.format(cnt - 1)))

    # connect to the selected vpn and write output to the command line
    print('Connecting to: {}'.format(vpn_locations[val - 1]))
    cmd = 'cd {}; openvpn --mute-replay-warnings --config "{}.ovpn" --auth-user-pass "{}"' \
        .format(ovpndir, vpn_locations[val - 1], credentials)
    p = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE)
    try:
        while True:
            out = p.stderr.read(1)
            if out == '' and p.poll() is not None:
                break
            else:
                sys.stdout.write(out).flush()
    except KeyboardInterrupt:  # look for <ctrl>c to exit & close vpn
        pass


if __name__ == '__main__':
    main(sys.argv[1:])
